## Development workflow: how to run the application locally

First set up your **environment variables** locally for you to be able to use your own Twitter API keys as follows:
```
CONSUMER_KEY=12345yourkeyhere
CONSUMER_SECRET=45678yourkeyhere
ACCESS_TOKEN_KEY=891ABCyourkeyhere
ACCESS_TOKEN_SECRET=111XYZyourkeyhere
```


Build the images and run the containers:
```
docker-compose up -d --build
```

Go to http://localhost/  

By default the app gets the tweets for the searchtag 'MITB', a professional wrestling pay-per-view event, 
because it was one of the trendiest topic at the time of the code implementation.  
Add your own searchtag and click the "Search" button to retrieve the most popular hashtags **by occurrence, per hours, per days and per weeks in the past 7 days**.  
Please note this is due to the **api 7-day limit**: *the Twitter Search API searches against a sampling of recent Tweets published in the past 7 days*.
You can get the corresponding data in json format at the below endpoints:
```
GET http://localhost/tweets/bitcoin
GET http://localhost/tweets/top-today-hashtags/bitcoin
GET http://localhost/tweets/top-last-week-hashtags/bitcoin
GET http://localhost/tweets/top-last-month-hashtags/bitcoin
```

To bring down the containers and remove the associated images:
```
docker-compose down --rmi all
```