import React, { Component } from "react";
import axios from "axios";

import "./App.css";
import SearchTweets from "./components/SearchTweets";
import TweetsList from "./components/TweetsList";
import TopTenPerHoursHashtagsList from "./components/TopTenPerHoursHashtagsList";
import TopTenPerDaysHashtagsList from "./components/TopTenPerDaysHashtagsList";
import TopTenPerWeeksHashtagsList from "./components/TopTenPerWeeksHashtagsList";

export const BoardItemDivStyle = {
    margin: '5px 0'
};

export const BoardItemContentDivStyle = {
    wordBreak: 'break-all',
    position: 'relative',
    padding: '20px',
    background: '#fff',
    borderRadius: '4px',
    textAlign: 'center',
    cursor: 'pointer',
    boxShadow: 'Opx 1px 3px 0 rgb(0 0 0 / 20%',
    margin: '5px 0'
};

class App extends Component {
  constructor() {
    super();
    this.state = {
      tweets: [],
      topTenPerHoursHashtags: [],
      topTenPerDaysHashtags: [],
      topTenPerWeeksHashtags: [],
      searchtag: "MITB",
      showSearching: false,
      isDisabled: false
    };
    this.submitSearch = this.submitSearch.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }
  componentDidMount() {
    const searchTag = this.state.searchtag;
    this.getTweets(searchTag);
    this.getTopTenPerHoursHashtagsList(searchTag);
    this.getTopTenPerDaysHashtagsList(searchTag);
    this.getTopTenPerWeeksHashtagsList(searchTag);
  }
  getTweets(searchTag) {
    axios
      .get(`${process.env.REACT_APP_TWEETS_SERVICE_URL}/tweets/${searchTag}`)
      .then(res => {
        console.log(res.data.data.tweets );
        this.setState({ tweets: res.data.data.tweets });
      })
      .catch(err => {
        console.log(err);
      });
  }
  getTopTenPerHoursHashtagsList(searchTag) {
    axios
      .get(`${process.env.REACT_APP_TWEETS_SERVICE_URL}/tweets/top-today-hashtags/${searchTag}`)
      .then(res => {
        console.log(res.data.data.top_10_hashtags_per_hours );
        this.setState({ topTenPerHoursHashtags: res.data.data.top_10_hashtags_per_hours });
      })
      .catch(err => {
        console.log(err);
      });
  }
  getTopTenPerDaysHashtagsList(searchTag) {
    axios
      .get(`${process.env.REACT_APP_TWEETS_SERVICE_URL}/tweets/top-last-week-hashtags/${searchTag}`)
      .then(res => {
        console.log(res.data.data.top_10_hashtags_per_days );
        this.setState({ topTenPerDaysHashtags: res.data.data.top_10_hashtags_per_days });
      })
      .catch(err => {
        console.log(err);
      });
  }
  getTopTenPerWeeksHashtagsList(searchTag) {
    axios
      .get(`${process.env.REACT_APP_TWEETS_SERVICE_URL}/tweets/top-last-month-hashtags/${searchTag}`)
      .then(res => {
        console.log(res.data.data.top_10_hashtags_per_weeks );
        this.setState({ topTenPerWeeksHashtags: res.data.data.top_10_hashtags_per_weeks });
      })
      .catch(err => {
        console.log(err);
      });
  }
  submitSearch(event) {
    event.preventDefault();
    console.log('sanity check!');
    this.setState({ showSearching: true });
    this.setState({ isDisabled: true });
    const data = {
      searchtag: this.state.searchtag
    };
    console.log(data);

    const url = `${process.env.REACT_APP_TWEETS_SERVICE_URL}/tweets`;
    axios.post(url, data)
         .then(res => {
            console.log('post sanity check!');
            const newState = {};
            newState.showSearching = false;
            newState.isDisabled = false
            newState.tweets = res.data.data.tweets;
            newState.topTenPerHoursHashtags = res.data.data.top_10_hashtags_per_hours ;
            newState.topTenPerDaysHashtags = res.data.data.top_10_hashtags_per_days;
            newState.topTenPerWeeksHashtags = res.data.data.top_10_hashtags_per_weeks;
            newState.searchtag = ""
            this.setState(newState);
         })
         .catch(err => {
          this.setState({ showSearching: false });
          this.setState({ isDisabled: false });
          console.log(err);
        });

  }
  handleChange(event) {
    const obj = {};
    obj[event.target.name] = event.target.value;
    this.setState(obj);
  }
  onChange(inputValue) {
    const newState = this.state.searchtag;
    newState.val = inputValue.target.value;
    this.setState(newState);
  };
  render() {
    return (
      <section className="section">

        <section className="hero is-info mb-5">
          <div className="hero-body">
            <div className="container mb-5">
              <div className="card">
                <div className="card-content">
                  <div className="content">
                    <SearchTweets
                      searchtag={this.state.searchtag}
                      showSearching={this.state.showSearching}
                      buttonIsDisabled={this.state.isDisabled}
                      submitSearch={this.submitSearch}
                      handleChange={this.handleChange}
                    />
                  </div>
                </div>
              </div>
            </div>
            <div className="columns is-mobile is-centered">
              <h1 className="title is-3">Twitter API Dashboard</h1>
            </div>
          </div>
        </section>

        <section className="container">
          <div className="level-item">
            <div className="columns is-multiline is-centered cards-container" id="sectioncontainer">
              <TweetsList tweets={this.state.tweets} />
              <TopTenPerHoursHashtagsList hashtagsPerHours={this.state.topTenPerHoursHashtags} />
              <TopTenPerDaysHashtagsList hashtagsPerDays={this.state.topTenPerDaysHashtags} />
              <TopTenPerWeeksHashtagsList hashtagsPerWeeks={this.state.topTenPerWeeksHashtags} />
            </div>
          </div>
        </section>
      </section>
    );
  }
}

export default App;