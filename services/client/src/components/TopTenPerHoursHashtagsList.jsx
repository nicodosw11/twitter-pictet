import React from 'react';

import { BoardItemDivStyle, BoardItemContentDivStyle } from "../App";

const TopTenPerHoursHashtagsList = (props) => {
  return (
    <div className="column is-one-quarter">
      <article className="message is-danger">
        <div className="message-header">
          <p className="is-size-7">Most popular #tags per hours</p>
        </div>
        <div className="message-body">
          {
            props.hashtagsPerHours.map((period) => {
              return (
                    <div className="board-item" style={BoardItemDivStyle} key={period.date}>
                      <div className="board-item-content" style={BoardItemContentDivStyle}>
                        <span className="is-size-7">{ period.date }</span>
                        <br />
                        {period.hashtags_stats.map((stat, i) => {
                            return (
                                <p className="is-size-7" key={i}>#{stat[0]} ({stat[1]})</p>
                            )})}
                      </div>
                    </div>
                    
              )
            })
          }
        </div>
      </article>
    </div>
  )
};

export default TopTenPerHoursHashtagsList;