import React from 'react';

const SearchTweets = (props) => {
  return (
        <div className="field has-addons">
            <div className="control has-icons-left has-icons-right flex-grow-1">
                <input 
                    type="search" 
                    name="searchtag"
                    className="input is-large" 
                    placeholder="Search for a particular topic (e.g. bitcoin, ethereum, nasa, python)"
                    required
                    value={props.searchtag}
                    onChange={props.handleChange}
                />
                <span className="icon is-medium is-left">
                <i className="fa fa-search" aria-hidden="true"></i>
                </span>
                <span className="icon is-medium is-right">
                <i className="fab fa-twitter" aria-hidden="true">
                </i>
                </span>
            </div>
            <div className="control">
                <button
                    onClick={props.submitSearch}
                    disabled={props.buttonIsDisabled}
                    className="button is-info is-large">
                Search
                </button>
                {props.showSearching &&
                <p>
                <span className="icon is-medium">
                    <i className="fas fa-spinner fa-pulse"></i>
                </span>
                <span className="search-text">Searching...</span>
                </p>
                }
            </div>
        </div>
  )
};

export default SearchTweets;                  
                  
                  
