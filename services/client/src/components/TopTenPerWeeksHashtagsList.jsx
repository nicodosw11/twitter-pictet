import React from 'react';

import { BoardItemDivStyle, BoardItemContentDivStyle } from "../App";

const TopTenPerWeeksHashtagsList = (props) => {
  return (
    <div className="column is-one-quarter">
      <article className="message is-info">
        <div className="message-header">
          <p className="is-size-7">Most popular #tags per weeks</p>
        </div>
        <div className="message-body">
          {
            props.hashtagsPerWeeks.map((period) => {
              return (
                    <div className="board-item" style={BoardItemDivStyle} key={period.date}>
                      <div className="board-item-content" style={BoardItemContentDivStyle}>
                        <span className="is-size-7">{ period.date }</span>
                        <br />
                        {period.hashtags_stats.map((stat, i) => {
                            return (
                                <p className="is-size-7" key={i}>#{stat[0]} ({stat[1]})</p>
                            )})}
                      </div>
                    </div>
                    
              )
            })
          }
        </div>
      </article>
    </div>
  )
};

export default TopTenPerWeeksHashtagsList;