import React from 'react';

import { BoardItemDivStyle, BoardItemContentDivStyle } from "../App";

const TweetsList = (props) => {
  return (
    <div className="column is-one-quarter">
      <article className="message is-black">
        <div className="message-header">
          <p className="is-size-7">Latest related tweets</p>
        </div>
        <div className="message-body">
          {
            props.tweets.map((tweet) => {
              return (
                    <div className="board-item" style={BoardItemDivStyle} key={tweet.id}>
                      <div className="board-item-content" style={BoardItemContentDivStyle}>
                        <span className="is-size-7">{ tweet.created_at }</span>
                        <br />
                        <small className="is-size-7">{ tweet.user }</small><br />
                        <small className="is-size-7">{ tweet.text }</small>
                      </div>
                    </div>
              )
            })
          }
        </div>
      </article>
    </div>
  )
};

export default TweetsList;