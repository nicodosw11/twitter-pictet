import sys

from flask.cli import FlaskGroup


from project import create_app


app = create_app()
# print(app.config, file=sys.stderr)
cli = FlaskGroup(create_app=create_app)


if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0")
