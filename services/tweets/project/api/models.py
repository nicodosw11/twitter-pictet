import os
import json
import tweepy

from dateutil.parser import parse
from datetime import datetime, timedelta
from collections import Counter


class SearchTweets:
    def __init__(self, searchtag="MITB"):
        self.searchtag = searchtag
        self.all_tweets = self.parse_with_twitter_api()

    def get_tweets_from_api(self):
        consumer_key = os.environ.get("CONSUMER_KEY")
        consumer_secret = os.environ.get("CONSUMER_SECRET")
        access_token = os.environ.get("ACCESS_TOKEN_KEY")
        access_token_secret = os.environ.get("ACCESS_TOKEN_SECRET")

        # authorize the API Key
        authentication = tweepy.OAuthHandler(consumer_key, consumer_secret)
        # authorization to user's access token and access token secret
        authentication.set_access_token(access_token, access_token_secret)
        # call the api
        api = tweepy.API(authentication, wait_on_rate_limit=True,wait_on_rate_limit_notify=True)

        # tweets = tweepy.Cursor(api.user_timeline, screen_name=self.searchtag).items(200)
        tweets = tweepy.Cursor(
            api.search, q=self.searchtag, result_type="popular", lang="en"
        ).items(200)
        return tweets

    def parse_with_twitter_api(self):
        raw_tweets = self.get_tweets_from_api()
        new_stack = [
            {
                "id": tweet._json["id"],
                "created_at": str(parse(tweet._json["created_at"])),
                "user": tweet._json["user"]["name"],
                "text": tweet._json["text"][:50] + ".."
                if len(tweet._json["text"]) > 50
                else tweet._json["text"],
                "hashtags": tweet._json["entities"]["hashtags"],
            }
            for tweet in raw_tweets
        ]
        sorted_stack = sorted(new_stack, key=lambda x: x["created_at"], reverse=True)
        return sorted_stack

    @property
    def hourly_timestamped_json(self):
        new_stack = self.all_tweets
        for tweet in new_stack:
            tweet["created_at"] = str(
                parse(tweet["created_at"]).replace(
                    microsecond=0, second=0, minute=0, tzinfo=None
                )
            )
        return new_stack

    @property
    def daily_timestamped_json(self):
        new_stack = self.all_tweets
        for tweet in new_stack:
            tweet["created_at"] = str(parse(tweet["created_at"]).date())
        return new_stack

    @property
    def weekly_timestamped_json(self):
        new_stack = self.all_tweets
        for tweet in new_stack:
            tweet["created_at"] = (parse(tweet["created_at"]).strftime("%Y-W%W"),)
        return new_stack

    def build_json(self, stack):
        dic = {}
        for item in stack:
            date = item["created_at"]
            hashtags = [i["text"] for i in item["hashtags"]]
            dic[date] = dic[date] + hashtags if date in dic else hashtags
        return dic

    @property
    def top10_tweets_json_per_hours(self):
        hourly_timed_json = self.hourly_timestamped_json
        dic = self.build_json(hourly_timed_json)
        return [
            {
                "date": datetime.strptime(k, "%Y-%m-%d %H:%M:%S").strftime(
                    "%a, %e %b %Y %I %p"
                ),
                "hashtags_stats": Counter(v).most_common()[:10],
            }
            for k, v in dic.items()
        ]

    @property
    def top10_tweets_json_per_days(self):
        daily_timed_json = self.daily_timestamped_json
        dic = self.build_json(daily_timed_json)
        return [
            {
                "date": datetime.strptime(k, "%Y-%m-%d").strftime("%a, %e %b %Y"),
                "hashtags_stats": Counter(v).most_common()[:10],
            }
            for k, v in dic.items()
        ]

    @property
    def top10_tweets_json_per_weeks(self):
        weekly_timed_json = self.weekly_timestamped_json
        dic = self.build_json(weekly_timed_json)
        return [
            {"date": k, "hashtags_stats": Counter(v).most_common()[:10]}
            for k, v in dic.items()
        ]

