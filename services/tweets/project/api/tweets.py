import json


from flask import Blueprint, request, jsonify
from flask_restful import Resource, Api


from project.api.models import SearchTweets


tweets_blueprint = Blueprint("tweets", __name__)
api = Api(tweets_blueprint)


class TweetsPing(Resource):
    def get(self):
        return {"status": "success", "message": "pong!"}


class TweetsPong(Resource):
    def get(self):
        return {"status": "success", "message": "ping!"}


class TweetsList(Resource):
    def get(self, searchtag):
        """Get all tweets"""

        search = SearchTweets()

        response_object = {"status": "success", "data": {"tweets": search.all_tweets}}

        return response_object, 200

    def post(self):
        """Search for tweets by user screen name"""

        post_data = request.get_json()
        response_object = {"status": "fail", "message": "Invalid payload."}
        if not post_data:
            return jsonify(response_object), 400
        searchtag = post_data.get("searchtag")
        search = SearchTweets(searchtag)
        result = {
            "status": "success",
            "data": {
                "tweets": search.all_tweets,
                "top_10_hashtags_per_hours": search.top10_tweets_json_per_hours,
                "top_10_hashtags_per_days": search.top10_tweets_json_per_days,
                "top_10_hashtags_per_weeks": search.top10_tweets_json_per_weeks,
            },
        }

        return jsonify(result)


class TweetsTopToday(Resource):
    def get(self, searchtag):
        """Get all tweets per hours"""

        search = SearchTweets(searchtag)

        response_object = {
            "status": "success",
            "data": {"top_10_hashtags_per_hours": search.top10_tweets_json_per_hours},
        }

        return response_object, 200


class TweetsTopLastWeek(Resource):
    def get(self, searchtag):
        """Get all tweets per days"""

        search = SearchTweets(searchtag)

        response_object = {
            "status": "success",
            "data": {"top_10_hashtags_per_days": search.top10_tweets_json_per_days},
        }

        return response_object, 200


class TweetsTopLastMonth(Resource):
    def get(self, searchtag):
        """Get all tweets per weeks"""

        search = SearchTweets(searchtag)

        response_object = {
            "status": "success",
            "data": {"top_10_hashtags_per_weeks": search.top10_tweets_json_per_weeks},
        }

        return response_object, 200


api.add_resource(TweetsPing, "/tweets/ping")
api.add_resource(TweetsPong, "/tweets/pong")
api.add_resource(TweetsList, "/tweets", endpoint='post_the_tweets_from_searchtag')
api.add_resource(TweetsList, "/tweets/<searchtag>", endpoint='get_the_tweets_from_searchtag')
api.add_resource(TweetsTopToday, "/tweets/top-today-hashtags/<searchtag>")
api.add_resource(TweetsTopLastWeek, "/tweets/top-last-week-hashtags/<searchtag>")
api.add_resource(TweetsTopLastMonth, "/tweets/top-last-month-hashtags/<searchtag>")

