import os


class BaseConfig:
    """Base configuration"""

    DEBUG_TB_ENABLED = False
    DEBUG_TB_INTERCEPT_REDIRECTS = False


class DevelopmentConfig(BaseConfig):
    """Development configuration"""

    TWEEPY_CONSUMER_KEY = os.environ.get("CONSUMER_KEY")
    TWEEPY_CONSUMER_SECRET = os.environ.get("CONSUMER_SECRET")
    TWEEPY_ACCESS_TOKEN_KEY = os.environ.get("ACCESS_TOKEN_KEY")
    TWEEPY_ACCESS_TOKEN_SECRET = os.environ.get("ACCESS_TOKEN_SECRET")
    DEBUG_TB_ENABLED = True
